<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller{
    public function index(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('UsuarioModel','model');
        $v['lista'] = $this->model->lista();
        $this->load->view('usuario/table_view', $v);

        $this->load->view('common/footer');

    }
//pode trocar o nome que funciona
    public function cadastro(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        //if(sizeof($_POST) > 0) print_r($_POST);
        $data['acao'] = "Enviar";
        $data['titulo'] = "Cadastro de Usuario";
    
        $this->load->model('UsuarioModel','model');//atalho para a funcao de baixo pode escrever apenas model
        $this->model->criar();//criar é o nome da funcao Usuario Model

        $this->load->view('usuario/form_cadastro',$data);
        $this->load->view('common/footer');

    }

    public function edit($id){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('UsuarioModel','model');
        $this->model->atualizar($id);

        $data['titulo'] = "Edição de Usuario";
        $data['acao'] = "Atualizar";
        $data['user'] = $this->model->carrega_usuario($id);
        //print_r($data['user']);

        $this->load->view('usuario/form_cadastro',$data);
        $this->load->view('common/footer');

    }

    public function delete($id){
        $this->load->model('UsuarioModel','model');
        $this->model->delete($id);
        redirect('usuario');
                                 
    }

}