<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Cadastro extends MY_Controller{

    public function index(){
        $this->load->model('CadastroModel','model');
        $this->model->salvar();

       $html =  $this->load->view('cadastro/form_dados_pessoais',null,true);
       $html .= $this->load->view('cadastro/form_endereco',null,true);
       $html .= $this->load->view('cadastro/form_redes_sociais',null,true);
       $this->show($html);
        
    }
    
    public function teste(){
        $html = "ola";
        $this->show($html);
    }

   

}

?>