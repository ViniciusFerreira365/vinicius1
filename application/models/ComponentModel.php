<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');
include APPPATH.'libraries/component/Table.php';
include APPPATH.'libraries/component/Panel.php';
include_once APPPATH.'libraries/User.php';

class ComponentModel extends CI_Model{

    private $color = array('primary','secondary','warning',
    'danger','info','light','dark','success');

    public function getPanelList(){
        $rs=$this->db->get('panel_data',8);//Limita a 10 elementos
        $v = $rs->result();
        $html = '';
        foreach ($v as $row ) {
        $panel = new Panel($row);//Cria um obejto da classe panel
        $panel->setCols(3);
        $panel->setColor($this->color[rand(0,7)]);
        $html .= $panel->getHTML();
        }
      
      
        return $html;
    }

    public function getTable(){
        $user = new User();
        $data = $user->getAll();
        $header = array('','Nome','Sobrenome','E-mail','Telefone','Senha','Criado em:');

        $table = new Table($data, $header);
        $table->set_header_color('elegant-color');
        $table->use_white_text();
        $table->zebra_table();
        $table->use_border();
        $table->use_hover();
        //$table->small_table();
        $table->mt(5);
        //$table->column_size(12);
        $table->use_action_button();
        
        return $table->getHTML();

    }

}



?>  