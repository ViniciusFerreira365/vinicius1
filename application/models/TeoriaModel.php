<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/teoria/Pessoa.php';
include_once APPPATH.'libraries/teoria/Aluno.php';


class TeoriaModel extends CI_Model{
    public function heranca(){
        $p1 = new Pessoa('Maria', 25);
        $p2 = new Pessoa('José', 28);
        var_dump($p1); echo '<br>';
        var_dump($p2); echo '<br><br>';
        echo "<br>A idade de ".$p1->getNome(). "é:".$p1->getIdade();
        echo "<br>A idade de ".$p2->getNome(). " é :".$p2->getIdade();

        $p1->setIdade(34);
        $p2->setIdade(38);
        echo"<br>Depois de executar a função setIdade<br>";
        echo "<br>A idade de ".$p1->getNome(). "é:".$p1->getIdade();
        echo "<br>A idade de ".$p2->getNome(). " é :".$p2->getIdade();

        echo"<br>Depois de executar a função getPressao<br>";
        echo "<br>A Pressao de  ".$p1->getNome(). " é:".$p1->getPressao();
        echo "<br>A Pressao de ".$p2->getNome(). " é :".$p2->getPressao();
        

        echo"<br>Depois de executar a função andar<br>";
        echo "<br>A Posição de  ".$p1->getNome(). " é:".$p1->andar();
        echo "<br>A Posição de ".$p2->getNome(). " é :".$p2->andar();
        echo'<br>';

        $a1 = new Aluno('Einstein', 22,'Turma de Sexta');
        //$a1->setTurma('LP2-  Sexta Feira');
        //var_dump($a1);
        

        echo"<br>Metodos do aluno<br>";
        echo "<br>A idade de  ".$a1->getNome(). " é:".$a1->getIdade();
        echo "<br>A Posição de ".$a1->getNome(). " é :".$a1->andar();
        echo "<br>A Pressao de ".$a1->getNome(). " é :".$a1->getPressao();
        echo "<br>A turma do ".$a1->getNome(). " é :x".$a1->getTurma();
        echo'<br>';

    }

}



?>