<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/User.php';//caminho do diretorio aplication em libraries

class UsuarioModel extends CI_Model{

    public function carrega_usuario($id){
        $user = new User();
        return $user->getById($id);

    }

    public function criar(){
        if(sizeof($_POST) == 0) return ;
        //$data = $this->input->post();
       //print_r($data);
         $nome = $this->input->post('nome');
         $email= $this->input->post('email');
         $senha = $this->input->post('senha');
         $sobrenome = $this->input->post('sobrenome');


         $user = new User($nome,$sobrenome,$email,$senha);//Objeto Copia da classe 
         $user->setTelefone($this->input->post('telefone'));//Para itens opcionais -> operador para executar funcao
         $user->save();
            
    }

    public function lista(){
        $html = '';
        $user = new User();
        // Organiza a lista e depois retorna o resultado
        $data =  $user->getAll();
        //print_r($data);
        $html .= '<table class  = "table">';
        foreach ($data as $row){
            $html .='<tr>';
            $html .='<td>' .$row['nome'].'</td>';
            $html .='<td>' .$row['sobrenome'].'</td>';
            $html .='<td>' .$row['email'].'</td>';
            $html .='<td>' .$row['telefone'].'</td>';
            $html .='<td>'.$this->get_edit_icon($row['id']).'</td></tr>';
        }
        $html .= '</table>';
        return $html;

    }


    private function get_edit_icon($id){//Funcao axuliar para alteracao de icones 
        $html = '';
        $html .=    '<a href="'.base_url('usuario/edit/'.$id).'"><i class="fas fa-edit mr-3 text-info"></i>';
        $html .= '  <a href="'.base_url('usuario/delete/'.$id).'"><i class="fas fa-times ml-3 text-danger"></i>';
        return $html;
    }


    public function atualizar($id){
        if(sizeof($_POST) == 0) return ;
        $data = $this->input->post();
         $user = new User();

         if($user->update($data, $id))
            redirect('usuario');
            
    }


    public function delete($id){
        $user = new User();
        $user->delete($id);
    }
}

 /*para ver se a funcao de cima funciona  imprimir os dados apenas essa classe acessa os dados
        Principio do privilegio minimo
        Classe'matriz para criar objeto' define atributos do objeto
        model trabalha com dados*/
?>

