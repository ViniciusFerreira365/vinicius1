<div class="container mt-3">
    <div class="card">
    <div class="card-header"><h4>Redes Socias</h4></div>

    <div class="form-row">
        <div class="col-md-6">
        <div class="md-form form-group">
            <input type="text" class="form-control" name="facebook" id="facebook"  value="<?= set_value('facebook') ?>">
            <label for="instagram">Facebook</label>
        </div>
        </div>


        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text" class="form-control" name="twitter" id="twitter"  value="<?= set_value('twitter') ?>">
            <label for="twitter">Twiter</label>
        </div>
        </div>

    </div>

    <div class="form-row">
        <div class="col-md-6">
        <div class="md-form form-group">
            <input type="text" class="form-control" name="instagram" id="instagram"  value="<?= set_value('instagram') ?>">
            <label for="instagram">Instagram</label>
        </div>
        </div>


        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text" class="form-control" name="linkedin" id="linkedin"  value="<?= set_value('linkedin') ?>">
            <label for="linkedin">Linkedin</label>
        </div>
        </div>

    </div>

    <button class="btn btn-info my-4 btn-block" type="submit">Enviar</button>
    </form>

    </div>
    </div>

</div>