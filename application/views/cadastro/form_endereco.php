<div class="container mt-3">


    <div class="card">
        <div class="card-header"><h4>Endereço</h4></div>
        <div class="card-body">
                

    <div class="form-row">

        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text" class="form-control" name="tipo_logradouro" id="tipo_logradouro" value="<?= set_value('tipo_logradouro') ?>" placeholder="Avenida, Rua, etc">
            <label for="tipo_logradouro">Tipo do logradouro</label>
        </div>
        </div>

        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text" class="form-control" name="nome_logradouro" id="nome_logradouro" value="<?= set_value('nome_logradouro') ?>" placeholder="Tiradentes, Paulista">
            <label for="nome_logradouro">Nome do logradouro</label>
        </div>
        </div>

    </div>

    <div class="row">

        <div class="col-md-2">
            <div class="md-form form-group">
                <input type="text" class="form-control" name="numero" id="numero" value="<?= set_value('numero') ?>" placeholder="Numero">
                <label for="numero">Numero</label>
            </div>
        </div>

        <div class="col-md-4">

            <div class="md-form form-group">
                <input type="text" class="form-control" name="complemento" id="complemento" value="<?= set_value('complemento') ?>" placeholder="Complemento">
                <label for="complemento">Complemento</label>
            </div>
        </div>

        <div class="col-md-4">

            <div class="md-form form-group">
                <input type="text" class="form-control" name="cep" id="cep" value="<?= set_value('cep') ?>" placeholder="00.000-000">
                <label for="cep">CEP</label>
            </div>
        </div>

    </div>

    <div class="form-row">

        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text" class="form-control" name="cidade" id="cidade"  value="<?= set_value('cidade') ?>"placeholder="São Paulo">
            <label for="cidade">Cidade</label>
        </div>
        </div>


        <div class="col-md-6">

        <div class="md-form form-group">
            <input type="text" class="form-control" name="estado" id="estado"  value="<?= set_value('estado') ?>" placeholder="11206-1117">
            <label for="estado">Estado</label>
        </div>
        </div>

    </div>

    

    </div>
    </div>

</div>