<div class="container">
    <div class="row mt-4">
        <div class="col-md-4 mx-auto">
        
                    <!-- Default form register -->
            <form method = "POST" class="text-center border border-light p-5">
            <p class = "h4 mb-4"> <?= $titulo ?></p>

            <p class="h4 mb-4"></p>
            <div class="form-row mb-4">
                <div class="col">
                    <!-- First name -->
                    <input type="text" value = "<?= isset($user['nome'])? $user['nome'] : '' ?>" name = "nome" id="nome" class="form-control" placeholder="Nome">
                </div>
                <div class="col">
                    <!-- Last name -->
                    <input type="text" value = "<?= isset($user['sobrenome'])? $user['sobrenome'] : '' ?>" name = "sobrenome" id="sobrenome" class="form-control" placeholder="Sobrenome">
                </div>
            </div>
            <!-- E-mail -->
            <input type="email" value = "<?= isset($user['email'])? $user['email'] : '' ?>" name = "email" id="email" class="form-control mb-4" placeholder="E-mail">
            <!-- Password -->
            <input type="password" value = "<?= isset($user['senha'])? $user['senha'] : '' ?>" name = "senha" id="senha" class="form-control" placeholder="Senha">
            <small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                A senha deve ter, pelo menos, oito caracteres.
            </small>

            <!-- Phone number -->
            <input type="text" value = "<?= isset($user['telefone'])? $user['telefone'] : '' ?>" name = "telefone" id="telefone" class="form-control" placeholder="Telefone">
            <small id="defaultRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">
               Opcional para a autenticação de dois fatore
            </small>

           

            <!-- Sign up button -->
            <button class="btn btn-info my-4 btn-block" type="submit"><?= $acao ?></button>

            
            

          

            </form>
            <!-- Default form register -->
        
        </div>
    </div>
</div>

