<div class="container">
    <div class="row">
        <div class="col-md-12 mt-4 mb-3">
            <h2 class="text-center">Cadastro dos Clientes - 2019</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <?= $formulario ?>
        </div>
        <div class="col-md-4 mt-5">
            <?= $card ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p class="red-text font-weight-bold text-center">Cuidado com o preenchimento correto deste formulário</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md 12 mt-4">
        <?= $jumbotron ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    </div>
</div>