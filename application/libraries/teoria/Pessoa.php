<?php

class Pessoa{
    // Atributos
    private $nome;
    private $idade;
    private $pressao;
    
    

    //Construtor informações iniciais da classe
    function __construct($nome,$idade){
        $this->idade = $idade;
        $this->nome = $nome;

    }

    //  Metodos acessores é publico (get/set ou getter/setter)
    public function getNome(){
        return $this->nome;
    }

    public function getIdade(){
        return $this->idade;
    }


    public function setIdade($idade){
        $this->idade = $idade;
        

    }

    public function getPressao(){
        $s = rand(5,25);
        $d = rand(3,15);
        return "$s x $d";

    }

    public function andar(){
        $x = rand(-30,30);
        $y = rand(-30,30);
        return "($x, $y)";

    }

}

?>