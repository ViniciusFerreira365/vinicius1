<?php
include_once 'Pessoa.php';


//Leia como: aluno é pessoa herança de pessoa
class Aluno extends Pessoa{
    private $turma;


    function __construct($nome,$idade,$turma){
        parent::__construct($nome,$idade);
        $this->turma = $turma;
    }

    public function setTurma($turma){
        $this->turma = $turma;
    }

    public function getTurma(){
        return $this->turma;
    }


}
?>