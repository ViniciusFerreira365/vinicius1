<?php
include_once 'Validator.php';
class RedesSociaisValidator extends Validator{
    public function getData(){
        $data['facebook'] = $this->input->post('facebook');
        $data['twitter'] = $this->input->post('twitter');
        $data['instagram'] = $this->input->post('instagram');
        $data['linkedin'] = $this->input->post('linkedin');
        return $data;

    }

    public function validate(){
        $this->form_validation->set_rules('facebook','facebook','trim|required');
        $this->form_validation->set_rules('twitter','twitter','trim|required');
        $this->form_validation->set_rules('instagram','instagram','trim|required');
        $this->form_validation->set_rules('linkedin','linkedin','trim|required');
    }
}




?>