<?php
include_once 'Validator.php';
class DadosPessoaisValidator extends Validator{

    public function validate(){
        $this->form_validation->set_rules('nome','nome do usuário','required|min_length[3]|max_length[15]');
        $this->form_validation->set_rules('sobrenome','sobrenome','required|min_length[2]|max_length[50]');
        $this->form_validation->set_rules('email','email','required|valid_email|is_unique[pessoa.email]');
        $this->form_validation->set_rules('senha','senha','required|min_length[8]');
        $this->form_validation->set_rules('nascimento','nascimento','required|max_length[10]');
    }
    
    public function getData(){
        $data['nome'] =  $this->input->post('nome');
        $data['sobrenome'] =  $this->input->post('sobrenome');
        $data['email'] =  $this->input->post('email');
        $data['senha'] =  $this->input->post('senha');
        $data['nascimento'] =  $this->input->post('nascimento');
        //Aqui  realizar ações corretivas sobre os dados
        return $data;
    }
    
}

?>