<?php
include_once 'Validator.php';
class EnderecoValidator extends Validator{
    public function validate(){
        $this->form_validation->set_rules('tipo_logradouro','logradouro','required|min_length[3]|max_length[100]');
        $this->form_validation->set_rules('nome_logradouro','nome','required|min_length[2]|max_length[101]');
        $this->form_validation->set_rules('numero','número','required|is_natural_no_zero|less_than[30000]');
        $this->form_validation->set_rules('complemento','complemento','min_length[3]|max_length[20]');
        $this->form_validation->set_rules('cep','Cep','required|exact_length[9]');
        $this->form_validation->set_rules('cidade','Cidade','required|max_length[28]');
        $this->form_validation->set_rules('estado','Estado','required|min_length[2]');
    }
    
    public function getData(){
        $data['tipo_logradouro'] =  $this->input->post('tipo_logradouro');
        $data['nome_logradouro'] =  $this->input->post('nome_logradouro');
        $data['numero'] =  $this->input->post('numero');
        $data['complemento'] =  $this->input->post('complemento');
        $data['cep'] =  $this->input->post('cep');
        $data['cidade'] =  $this->input->post('cidade');
        $data['estado'] =  $this->input->post('estado');
        //Aqui  realizar ações corretivas sobre os dados
        return $data;
    }


    
}



?>