<?php 
include_once 'DAO.php';
class Endereco extends DAO{
    function __construct(){
        parent::__construct('endereco');
    }
    // Sobre escrita de metodo: cria na classe filha um metodo 
    // que tem o mesmo nome de outro defenido na classe pai
    // para especializar algum comportamento ex id_pessoa = 0
    public function salvar($data,$id_pessoa = 0){
        $data['id_pessoa'] = $id_pessoa;
        parent::salvar($data);
        
    }

    
}


?>