<?php
include_once 'DAO.php';
class RedesSociais extends DAO{

    function __construct(){
        parent::__construct('redes_sociais');
    }

    public function salvar($data, $id_pessoa = 0){
        $data['id_pessoa'] = $id_pessoa;
        parent::salvar($data);
       
    }
}

?>