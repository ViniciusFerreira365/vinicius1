<?php

abstract class Validator extends CI_Object{
    
    public abstract function getData();
    public abstract function validate();
}

?>