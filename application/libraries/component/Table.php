<?php
/**
 *  Abstração de dados atributos, metodos, documentação o que faz recebe
 * e retorna?
 * podemos modelar a partir da abstração
 * 
 */

class Table{
    /**Matriz que contem os dados da tabela */
    private $data;
    /* Array de labels para a primeira linha da tabela*/
    private $header;
    /*Define a cor de fundo do cabeçalho da tabela*/
    private $header_color = '';
    /**Decide se a cor do texto sera branca */
    private $white_text = '';
    /**Centraliza */
    private $center_table = 'mx-auto';

    /**Lista de classe da tag table */
    private $table_classes = array('table', 'mx-auto');

    private $use_action_button = false;

    /**
     * Construtor da classe Table
     * @param array data: vetor associativo
     */
    function __construct(array $data, array $header){
        $this->data = $data;
        $this->header = $header;

    }
    /** 
     * Gera o codigo da Tabela.
     * @return string: codigo HTML da Tabela.
     */
    public function getHTML(){
        $html = '<table class = "'.$this->get_classes().' '.$this->center_table.' ">';
        $html .= $this->header();
        $html .=$this->body();
        $html .='</table>';
        return $html;
    }
    /*
    * Para documentacao privada 1 estrela nao aparecer na documentacao
    *Gera o cabeçalho da tabela
    *@return string:codigo HTMLL
    */
    private function header(){
        $html = '<thead class = "'.$this->header_color.' '.$this->white_text.'"><tr>';
       

        foreach($this->header as $col){
            $html .='<th scope="col">' .$col. '</th>';
        }
        //Se usar botoes de acao acrescente mais uma coluna
        if($this->use_action_button){
            $html .='<th scope="col">' .$col. '</th>';
        }
        $html .= '</tr></thead>';
        return $html;
    }

    public function body(){
        $html = '<tbody>';

        foreach($this->data as $row){
            $html .='<tr>';
            foreach($row as $col){
                $html .='<td>'.$col.'</td>';
            }
            if($this->use_action_button){
                $html .='<td>Lista de Botoes</td>';
            }
            $html .='</tr>';
        }
        $html .='</tbody>';
        return $html;
    }

    public function get_classes(){
        return implode(' ',$this->table_classes);
    }

    /**
     * Define a cor de fundo do cabeçalho da tabela.
     * @param string color: a cor a ser utilizada.
     */
    public function set_header_color($color){
        $this->header_color = $color;

    }
    /**
     * Força o uso de letra branca no cabeçalho nao recebe e nao retorna
     */
    public function use_white_text(){
        $this->white_text = 'text-white';
    }
   
    public function zebra_table(){
        $this->table_classes[] = 'table-striped';
    }

    public function use_border(){
        $this->table_classes[] = 'table-bordered';
    }

    public function use_hover(){
        $this->table_classes[] = 'table-hover';
    }

    public function small_table(){
        $this->table_classes[] = 'table-sm';
    }

    public function column_size($num = 12){
        if($num > 0 && $num <= 12)
        $this->table_classes[] = 'col-md-'.$num;

    }

    public function use_action_button(){
        $this->use_action_button = true;
    }

    public function aling_left(){
        $this->center_table = '';
    }

    public function mt($num){
        if($num > 0 && $num <= 5)
        $this->table_classes[] = 'mt-'.$num;
    }

    public function mr($num){
        if($num > 0 && $num <= 5)
        $this->table_classes[] = 'mr-'.$num;
    }

    public function mb($num){
        if($num > 0 && $num <= 5)
        $this->table_classes[] = 'mb-'.$num;
    }

    public function ml($num){
        if($num > 0 && $num <= 5)
        $this->table_classes[] = 'ml-'.$num;
    }

   

   
}

?>