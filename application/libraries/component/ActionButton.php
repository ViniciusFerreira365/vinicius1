<?php

class ActionButton{
    private $url;
    private $icon;
    private $color;
    private $title;
    /** */
    function __construct($icon, $url = '', $color = '', $title = ''){
        $this->$title = $title;
        $this->$color = $color;
        $this->$icon = $icon;
        $this->$url = $url;

    }

    public function getHTML(){
      $html .= '<a href ="'.base_url($this->url).'"title="'.$this->title.'">';
      $html .= '<i class ="far fa- '.$this->icon.'mr-3 text-'.$this->color.'"></i></a>';
      

        return $html;
    }
}

?>